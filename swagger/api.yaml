swagger: "2.0"
info:
  description: "This is a service both for TAP proteges and TAP leads. Protege can register their rotation preferences and view generated schedules. For the leads, this service enables them to have a summary of upcoming rotations and performs administrative tasks i.e. user management and schedule generation"
  version: "1.0.0"
  title: "TAP Service"
  contact:
    email: "SJTSOONJ@astro.com.my"
  license:
    name: "MIT"
    url: "https://opensource.org/licenses/MIT"
host: "localhost:3000"
basePath: "/api/v1"
tags:
- name: "schedule"
  description: "Schedules rotations"
- name: "config"
  description: "Configures properties of available rotations in the program"
- name: "user"
  description: "Manages users"
schemes:
- "http"
paths:
  /schedule:
    post:
      tags:
      - "schedule"
      summary: "Generates a new schedule"
      description: ""
      operationId: "createSchedule"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - in: "body"
        name: "body"
        required: true
        schema:
          $ref: "#/definitions/RequestSchedule"
      responses:
        200:
          description: "Request to generate schedule successfully sent"
        405:
          description: "Invalid input"
    get:
      tags:
      - "schedule"
      summary: "Get the latest schedule"
      description: ""
      operationId: "getSchedule"
      produces:
      - "application/json"
      responses:
        200:
          description: "Success"
          schema:
            type: "array"
            items:
              $ref: "#/definitions/Schedule"
        404:
          description: "No schedules found"
  /config:
    post:
      tags:
      - "config"
      summary: "Add a new rotation config"
      description: ""
      operationId: "addRotationConfig"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - in: "body"
        name: "body"
        required: true
        schema:
          type: "array"
          items:
            $ref: "#/definitions/RotationConfig"
      responses:
        200:
          description: "Rotation config successfully added"
        405:
          description: "Invalid input"
    get:
      tags:
      - "config"
      summary: "Lists all rotation config"
      description: ""
      operationId: "listRotationConfig"
      produces:
      - "application/json"
      responses:
        200:
          description: "Success"
          schema:
            type: "array"
            items:
              $ref: "#/definitions/RotationConfig"
        404:
          description: "Rotation not found"
  /config/{rotationID}:
    put:
      tags:
      - "config"
      summary: "Update an existing rotation config"
      description: ""
      operationId: "updateRotationConfig"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - in: "body"
        name: "body"
        required: true
        schema:
          $ref: "#/definitions/RotationConfig"
      - name: "rotationID"
        in: "path"
        description: "ID of rotation to return"
        required: true
        type: "integer"
        format: "int64"
      responses:
        200:
          description: "Rotation successfully updated"
        404:
          description: "Rotation not found"
        405:
          description: "Invalid input"
    delete:
      tags:
      - "config"
      summary: "Delete an existing rotation config"
      description: ""
      operationId: "deleteRotationConfig"
      produces:
      - "application/json"
      parameters:
        - name: "rotationID"
          in: "path"
          description: "ID of rotation to return"
          required: true
          type: "integer"
          format: "int64"
      responses:
        200:
          description: "Rotation successfully deleted"
        404:
          description: "Rotation not found"
  /user:
    post:
      tags:
      - "user"
      summary: "Create user"
      description: "This can only be done by the admin."
      operationId: "createUser"
      produces:
      - "application/json"
      parameters:
      - in: "body"
        name: "body"
        description: "Created user object"
        required: true
        schema:
          $ref: "#/definitions/User"
      responses:
        200:
          description: "User successfully created"
        409:
          description: "User already exists"
    get:
      tags:
      - "user"
      summary: "Lists all users"
      description: ""
      operationId: "listUser"
      produces:
      - "application/json"
      responses:
        200:
          description: "Success"
          schema:
            type: "array"
            items:
              $ref: "#/definitions/User"
        404:
          description: "User not found"
  /user/{userID}:
    get:
      tags:
      - "user"
      summary: "Get information for a user"
      description: ""
      operationId: "getUser"
      produces:
      - "application/json"
      parameters:
      - name: "userID"
        in: "path"
        description: "ID of user to return"
        required: true
        type: "integer"
        format: "int64"
      responses:
        200:
          description: "Success"
          schema:
            type: "array"
            items:
              $ref: "#/definitions/User"
        404:
          description: "User not found"
    put:
      tags:
      - "user"
      summary: "Update an existing user details"
      description: ""
      operationId: "updateUser"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - in: "body"
        name: "body"
        required: true
        schema:
          $ref: "#/definitions/User"
      - name: "userID"
        in: "path"
        description: "ID of user to return"
        required: true
        type: "integer"
        format: "int64"
      responses:
        200:
          description: "User successfully updated"
        404:
          description: "User not found"
        405:
          description: "Invalid input"
securityDefinitions:
  api_key:
    type: "apiKey"
    name: "X-API-KEY"
    in: "header"
definitions:
  User:
    type: "object"
    required:
    - "username"
    - "password"
    - "displayName"
    - "joinDate"
    properties:
      username:
        type: "string"
        minLength: 8
        maxLength: 8
      displayName:
        type: "string"
        minLength: 1
        maxLength: 20
      password:
        type: "string"
        format: "password"
      joinDate:
        type: "string"
        format: "date"
      electives:
        type: "array"
        items:
          type: "string"
      status:
        type: "string"
        description: "User Status"
        enum:
        - "active"
        - "completed"
        - "suspended"
      mentor:
        type: "string"
  RotationConfig:
    type: "object"
    properties:
      name:
        type: "string"
      duration:
        type: "integer"
        format: "int64"
        description: "Duration of rotation in weeks"
      category:
        type: "string"
        enum:
        - "core"
        - "elective"
      capacity:
        type: "integer"
        format: "int64"
        minimum: 1
        description: "Maximum number of concurrent proteges allowed in a rotation"
      championName:
       type: "string"
      championEmail:
       type: "string"
  RequestSchedule:
    type: "object"
    required:
    - "startDate"
    properties:
      initSchedule:
        type: "array"
        items:
            $ref: "#/definitions/Schedule"
      startDate:
        type: "string"
        format: "date"
      endDate:
        type: "string"
        format: "date"
  Schedule:
    type: "object"
    required:
    - "userId"
    - "rotationId"
    properties:
      userId:
        type: "string"
      rotationId:
        type: "string"
      startDate:
        type: "string"
        format: "date"
      endDate:
        type: "string"
        format: "date"
externalDocs:
  description: "Find out more about TAP Service"
  url: "https://bitbucket.org/parapatrp/tap-management-service"
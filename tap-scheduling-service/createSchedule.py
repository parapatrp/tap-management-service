#!/usr/bin/python
import boto3
import os
import logging
import numpy as np
import time
import uuid

from utils import generateBlock
from boto3.dynamodb.conditions import Key
from constraint import Problem, AllDifferentConstraint
from datetime import datetime, timedelta

# Logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)

DATE_FORMAT = "%m%Y"
DISPLAY_DATE_FORMAT = "%d%m%Y"
top_per_block = 2


def mapRotations(rotations, startYear=2017, endYear=2020):
    # Maps rotation config from DynamoDB to format that can be used by the scheduler
    fmt = {
        x["pK"]: Rotation(
            x["pK"],
            x["category"] == "core",
            generateBlock([str(x) for x in range(startYear, endYear)]),
            int(x["capacity"]),
            int(x["duration"]),
        )
        for x in rotations
    }
    return fmt


def mapProteges(proteges, schedules):
    # Maps users from DynamoDB to format that can be used by the scheduler
    fmt = {
        x["pK"]: Protege(
            x["pK"],
            mapScheduleFromDB(schedules[x["pK"]]),
            x["electives"],
            datetime.strptime(x["joinDate"], "%Y-%m-%d").strftime(DISPLAY_DATE_FORMAT),
        )
        for x in proteges
    }
    return fmt


def mapScheduleFromDB(schedule):
    # Maps users from DynamoDB to format that can be used by the scheduler
    fmt = [
        (
            x["rotationId"],
            (
                datetime.strptime(x["startDate"], "%Y-%m-%d").strftime(
                    DISPLAY_DATE_FORMAT
                ),
                datetime.strptime(x["endDate"], "%Y-%m-%d").strftime(
                    DISPLAY_DATE_FORMAT
                ),
            ),
        )
        for x in schedule
    ]
    return fmt


def mapScheduleToDB(schedule):
    fmt = [
        {
            "rotationId": x[0],
            "startDate": datetime.strptime(x[1][0], DISPLAY_DATE_FORMAT).strftime(
                "%Y-%m-%d"
            ),
            "endDate": datetime.strptime(x[1][1], DISPLAY_DATE_FORMAT).strftime(
                "%Y-%m-%d"
            ),
        }
        for x in schedule
    ]
    return fmt


def handler(event, context):
    # AWS clients
    dynamodb = boto3.resource("dynamodb", region_name="ap-southeast-1")
    table = dynamodb.Table(os.environ["AWS_DYNAMODB_TABLE"])
    index_name = os.environ["INDEX_NAME"]

    try:
        createdAt = datetime.utcnow().isoformat()
        schedules = event["initSchedule"]
        isManual = event["isManual"]

        users = table.query(
            IndexName=index_name, KeyConditionExpression=Key("sK").eq("USER")
        )["Items"]

        proteges = mapProteges(
            list(
                filter(
                    lambda user: user["role"]
                    and user["role"] == "regular"
                    and user["joinDate"] and user['status'] == "active",
                    users,
                )
            ),
            schedules,
        )

        # Sort Proteges according to join date
        protegeList = sorted(proteges.values(), key=lambda x: x.joinDate)
        startYear = protegeList[0].joinDate.year
        endYear = protegeList[-1].joinDate.year + 3

        logger.info("Fetched all users")

        rotations = mapRotations(
            table.query(
                IndexName=index_name, KeyConditionExpression=Key("sK").eq("ROTATION")
            )["Items"], startYear, endYear
        )

        if not isManual:
            logger.info("Start generating schedule")
            schedules, _, rotations = generateSchedule(proteges.values(), rotations)
            logger.info("Completed schedule generation")
            schedules = {
                userId: mapScheduleToDB(schedule)
                for userId, schedule in schedules.items()
            }
        conflictAssignments = getConflictAssignments(rotations)
        logger.info("Completed conflict detection")
        logger.info(conflictAssignments)
        table.put_item(
            Item={
                "pK": str(uuid.uuid4()),
                "sK": "PENDING_SCHEDULE",
                "data": f"{str(len(conflictAssignments) > 0).lower()}#{createdAt}",
                "schedule": schedules,
                "isConflict": len(conflictAssignments) > 0,
                "conflictAssignments": conflictAssignments,
                "createdAt": createdAt,
            }
        )
        table.put_item(
            Item={
                "pK": "latestSchedule",
                "sK": "SCHEDULE",
                "data": f"{str(len(conflictAssignments) > 0).lower()}#{createdAt}",
                "schedule": schedules,
                "isConflict": len(conflictAssignments) > 0,
                "conflictAssignments": conflictAssignments,
                "createdAt": createdAt,
            }
        )
        logger.info("Updated schedule table")
        return {"statusCode": 200}
    except Exception as e:
        logger.error(e)
        return {"statusCode": 400}


class Rotation:
    def __init__(self, name, is_core, blocks, capacity, duration):
        self.name = name
        self.is_core = is_core
        self.blocks = blocks
        self.capacity = capacity
        self.duration = duration
        self.assignments = []

    def score(self, assignments):
        """All blocks must have a staffer assigned"""
        assigned_blocks = [b for b, _, _ in assignments]
        for block in self.blocks:
            if block not in assigned_blocks:
                return -1
        for block, task, staffers in assignments:
            if len(staffers) != self.nstaffers:
                return -1
        return self.task_score(assignments)

    def getConflictAssignments(self):
        conflict_assignments = []
        for date, capacity in self.blocks.items():
            if capacity > self.capacity:
                conflict_assignments += [
                    {"userId": x[0], "startDate": x[1]}
                    for x in list(filter(lambda x: x[1][2:] == date, self.assignments))
                ]
        return conflict_assignments

    def valid(self, staffer):
        return self.task_valid(staffer)

    def task_score(self, assignments):
        return 0

    def task_valid(self, staffer):
        return True

    def __repr__(self):
        return self.name


class Protege:
    def __init__(self, name, schedule, electives, joinDate="01012018"):
        self.name = name
        self.schedule = schedule
        self.electives = electives
        self.joinDate = datetime.strptime(joinDate, DISPLAY_DATE_FORMAT)
        logger.info(f"Name: {name} {schedule}")

    def valid(self, task, block):
        return block in self.available_blocks and task.valid(self)

    def earliestAvailableDate(self):
        # Returns earliest available date to start rotation based of existing
        # schedule or return join date if no schedule were generated
        if len(self.schedule) == 0:
            return self.joinDate
        else:
            date = sorted(
                self.schedule,
                key=lambda x: datetime.strptime(x[1][1], DISPLAY_DATE_FORMAT),
                reverse=True,
            )[0][1][1]
            return datetime.strptime(date, DISPLAY_DATE_FORMAT)

    def __repr__(self):
        return self.name


def scoreConflict(rotations, protege_rotations, protege):
    # Generate starting date for each department
    date = protege.earliestAvailableDate()
    score = 0
    for assignment, _ in protege_rotations:
        rotation = rotations[assignment]
        score += rotation.blocks[date.strftime(DATE_FORMAT)]
        for m in range(rotation.duration):
            score += rotation.blocks[date.strftime(DATE_FORMAT)]
            date = date + timedelta(weeks=4)
    return score


def updateRotationBlock(rotations, solutions, protege):
    period = []
    # Assuming we use 1 month as minimal unit of time
    date = protege.earliestAvailableDate()
    for r in solutions:
        curr_rotation = rotations[r]
        rotations[r].assignments.append(
            (protege.name, date.strftime(DISPLAY_DATE_FORMAT))
        )
        start_date = date.strftime(DISPLAY_DATE_FORMAT)
        end_date = (
            date + timedelta(weeks=4 * curr_rotation.duration) - timedelta(days=1)
        )
        # Make sure the date falls on weekday
        if end_date.weekday() >= 5:
            end_date -= timedelta(end_date.weekday() - 4)
        period.append((start_date, end_date.strftime(DISPLAY_DATE_FORMAT)))
        for m in range(curr_rotation.duration):
            key = date.strftime(DATE_FORMAT)
            date += timedelta(weeks=4)
            if date.strftime(DATE_FORMAT) != key:
                rotations[r].blocks[key] += 1
    return period, rotations


def score_solution(rotations, solution, protege):
    # Score has to be negative because argsort sort returns in descending order
    spread_first_constraint = 0
    # SOFT CONSTRAINT 1 - Core rotations should be completed in the first 4 rotations if possible
    core_first_constraint = sum(
        [-3 if rotations[x[0]].is_core else 0 for x in solution if int(x[1]) <= 4]
    )

    # SOFT CONSTRAINT 2 - Prefer rotation with less number of TAP
    spread_first_constraint = scoreConflict(rotations, solution, protege)

    # SOFT CONSTRAINT 3 - External Assignment must be assigned last
    external_assignment_constraint = (
        99 if "EXT" in [x[0] for x in solution] and solution[-1][0] != "EXT" else 0
    )
    return (
        core_first_constraint + spread_first_constraint + external_assignment_constraint
    )


def generateScheduleFromProtege(proteges):
    return {protege.name: protege.schedule for protege in proteges}


def initRotationFromProtege(proteges, rotations):
    for p in proteges:
        for r, dates in p.schedule:
            rotations[r].blocks[dates[0][2:]] += 1
            rotations[r].assignments.append((p.name, dates[0]))
    return rotations


def getConflictAssignments(rotations):
    # Given rotations return block of time with conflict
    return {
        r.name: r.getConflictAssignments()
        for r in rotations.values()
        if len(r.getConflictAssignments()) > 0
    }


def shouldSchedule(protege, rotations, min_duration=24):
    # Check if scheduling is needed for the protege
    # @param min_duration: minimum months needed to complete the program
    return (
        sum(
            [
                rotations[rotationName].duration
                for rotationName, startDate in protege.schedule
            ]
        )
        < min_duration
    )


def generateSchedule(proteges, rotations,
                     config={'CORE_ROTATIONS': ["PMO", "PM", "SE", "PE"]}):

    rotations = initRotationFromProtege(proteges, rotations)
    schedules = {protege.name: protege.schedule for protege in proteges}
    start_time = time.time()

    for protege in filter(lambda p: shouldSchedule(p, rotations), proteges):
        print(f"Bee bop bee bop ... creating schedule for {protege.name}")
        print(f"Existing schedule: {protege.schedule}")

        problem = Problem()
        # List of rotations that can be rotated by current protege based on electives
        protege_rotations = list(
            filter(
                lambda x: x.name in (config['CORE_ROTATIONS'] + protege.electives),
                rotations.values(),
            )
        )
        protege_rotations = [
            x.name
            for x in protege_rotations
            if x.name not in [i[0] for i in protege.schedule]
        ]
        order = [
            str(i)
            for i in range(
                len(protege.schedule) + 1,
                len(protege_rotations) + len(protege.schedule) + 1,
            )
        ]
        print(f"Possible rotations: {protege_rotations}")
        print(f"Possible order: {order}")

        if len(protege_rotations) > 0:
            problem.addVariables(protege_rotations, order)

            # HARD CONSTRAINT 1 - All rotations must be unique in the period of the program
            problem.addConstraint(AllDifferentConstraint(), protege_rotations)

            solutions = problem.getSolutions()
            # Score the options
            print(f"Scoring {len(solutions)} found")
            scores = np.zeros(len(solutions))
            for sidx, solution in enumerate(solutions):
                scores[sidx] = score_solution(
                    rotations, sorted(solution.items(), key=lambda x: x[1]), protege
                )

            print("Finding top solutions")
            chosen = np.argsort(scores)
            ntied = (scores == scores[chosen[0]]).sum()
            if ntied > top_per_block:
                chosen = chosen[:ntied]
                np.random.shuffle(chosen)
            top_solutions = [solutions[i] for i in chosen[:top_per_block]]
            best_solution = top_solutions[0]
            print(best_solution)
            print(
                score_solution(
                    rotations,
                    sorted(best_solution.items(), key=lambda x: x[1]),
                    protege,
                )
            )
            print(top_solutions[1])
            print(
                score_solution(
                    rotations,
                    sorted(top_solutions[1].items(), key=lambda x: x[1]),
                    protege,
                )
            )
            print(rotations)

            period, rotations = updateRotationBlock(
                rotations,
                [x[0] for x in sorted(best_solution.items(), key=lambda x: x[1])],
                protege,
            )
            schedule = list(
                zip(
                    [x[0] for x in sorted(best_solution.items(), key=lambda x: x[1])],
                    period,
                )
            )
            protege.schedule += schedule
        schedules[protege.name] = protege.schedule
        print(f"{protege.name}'s schedule: {protege.schedule}")
        print(f"--- {(time.time() - start_time) / 60.0:.2f} mins elapsed ---")

    print(f"--- Total time taken: {(time.time() - start_time) / 60.0:.2f} mins ---")
    return schedules, proteges, rotations

## Requirements

- Run scheduler within bounded time range
- Run scheduler based on initial schedule. If not initial schedule is provided, latest working schedule will be used
- Scheduler to be triggered automatically every month (after electives have been updated)
    - shall not be ran if there no changes (no new proteges or electives change)
- Update schedule array in user object once a schedule is confirmed
- Convert a schedule into working schedule
- Schedule should be able to detect changes when a user is suspended
- What happen if initial schedule contains user that does not exist in the db

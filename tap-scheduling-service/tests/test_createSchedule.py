import os
import pytest
from createSchedule import *


@pytest.fixture(autouse=True)
def setup_teardown():
    os.environ["AWS_DYNAMODB_TABLE"] = "test-portal"
    os.environ["INDEX_NAME"] = "test-index"


@pytest.fixture()
def getUsers():
    return [
        {
            "displayName": "Soon Jin Tan",
            "role": "regular",
            "data": "active#2017-08-07",
            "status": "active",
            "electives": ["BRO", "INO", "SEC", "SYS", "ARC"],
            "joinDate": "2017-08-07",
            "sK": "USER",
            "mentorEmail": "george_au@astro.com.my",
            "pK": "sjtsoonj",
            "mentorName": "George Au",
        },
        {
            "displayName": "Thavakumaran Jeyananthan",
            "role": "regular",
            "data": "active#2017-08-15",
            "status": "active",
            "electives": ["BRO", "ANA", "SEC", "EXT", "ARC"],
            "joinDate": "2017-08-15",
            "sK": "USER",
            "mentorEmail": "piramila_poovan@astro.com.my",
            "pK": "jtthavaku",
            "mentorName": "Eva Poovan",
        },
        {
            "displayName": "Brianna Chang",
            "role": "regular",
            "data": "active#2017-09-04",
            "status": "active",
            "electives": ["BRO", "INO", "SYS", "TFM"],
            "joinDate": "2017-09-04",
            "sK": "USER",
            "pK": "cpspeish",
            "mentorEmail": "peter_koay@astro.com.my",
            "mentorName": "Peter Koay",
        },
    ]


@pytest.fixture()
def getRotationConfig():
    return [
        {
            "championName": "Ahmad Yunus",
            "capacity": 2,
            "data": "core",
            "championEmail": "ahmad_yunus@astro.com.my",
            "category": "core",
            "sK": "ROTATION",
            "pK": "PM",
            "duration": 3,
            "name": "Product Management",
        },
        {
            "championName": "Ajatshatru Singh",
            "capacity": 2,
            "data": "core",
            "championEmail": "ajatshatru_singh@astro.com.my",
            "category": "core",
            "sK": "ROTATION",
            "pK": "SE",
            "duration": 3,
            "name": "Software Engineering",
        },
        {
            "championName": "Michael Fu",
            "capacity": 2,
            "data": "core",
            "championEmail": "michael_fu@astro.com.my",
            "category": "core",
            "sK": "ROTATION",
            "pK": "PE",
            "duration": 3,
            "name": "Product Engineering",
        },
    ]


def test_mapRotations(getRotationConfig):
    assert 1 == 1

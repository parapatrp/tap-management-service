import itertools
from datetime import datetime

def generateBlock(years, units=[f"{x:02}" for x in range(1, 13)]):
    '''
    @param years: duration of schedule
    @param units: minimum unit of time that can be assigned (default to 1 month)
    '''
    print(years)
    block_time = [''.join(x) for x in itertools.product(units, years)]
    return {x: 0 for x in block_time}


const AWS = require('aws-sdk');
const { listWorkingSchedule, getSchedule } = require('./utils');

const lambda = new AWS.Lambda({region: 'ap-southeast-1'});

module.exports.handler = async (event, context, callback) => {
    console.log("HELLO")
    const { initSchedule, isManual = false } = JSON.parse(event.body)
    const { Item: workingSchedule } = await getSchedule('latestSchedule');
    const params = {
        FunctionName: process.env.SCHEDULE_LAMBDA,
        InvocationType: 'Event',
        Payload: JSON.stringify({
            initSchedule: Object.keys(initSchedule).length > 0 ? initSchedule : workingSchedule.schedule,
            isManual: isManual
        })
    }
    let res = {}
    try {
        let invokedRes = await lambda.invoke(params).promise()
        res = {
            statusCode: 200,
            headers: {
                "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
            },
            body: JSON.stringify({
                msg: 'Request to generate schedule has been received',
                result: params
            })
        }
        callback(null, res)
    } catch(err) {
        console.log(err)
        res = {
            statusCode: 400,
            body: JSON.stringify(err)
        }
        callback(res)
    }
};

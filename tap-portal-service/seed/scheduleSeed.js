seed = [
    // Schedule
    {
        pK: 'latestSchedule',
        sK: 'SCHEDULE',
        data: 'noconflict#2017-01-01',
        startDate: '2017-01-01',
        status: 'noconflict',
        schedule: []
    },
]

module.exports = seed;

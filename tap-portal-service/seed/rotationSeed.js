seed = [
    // Rotation Config
    {
        pK: 'PMO',
        sK: 'ROTATION',
        data: 'core',
        name: 'Project Management',
        duration: 3,
        category: 'core',
        capacity: 8,
        championName: 'Siew Mun',
        championEmail: 'siew-mun_chow@astro.com.my'

    },
    {
        pK: 'PM',
        sK: 'ROTATION',
        data: 'core',
        name: 'Product Management',
        duration: 3,
        category: 'core',
        capacity: 2,
        championName: 'Ahmad Yunus',
        championEmail: 'ahmad_yunus@astro.com.my'

    },
    {
        pK: 'PE',
        sK: 'ROTATION',
        data: 'core',
        name: 'Product Engineering',
        duration: 3,
        category: 'core',
        capacity: 2,
        championName: 'Michael Fu',
        championEmail: 'michael_fu@astro.com.my'

    },
    {
        pK: 'SE',
        sK: 'ROTATION',
        data: 'core',
        name: 'Software Engineering',
        duration: 3,
        category: 'core',
        capacity: 2,
        championName: 'Ajat',
        championEmail: 'ajatshatru_singh@astro.com.my'

    },
    {
        pK: 'ANA',
        sK: 'ROTATION',
        data: 'elective',
        name: 'Analytics',
        duration: 2,
        category: 'elective',
        capacity: 2,
        championName: 'Mark',
        championEmail: 'mark_meta@astro.com.my'

    },
    {
        pK: 'ARC',
        sK: 'ROTATION',
        data: 'elective',
        name: 'Architecture',
        duration: 3,
        category: 'elective',
        capacity: 2,
        championName: 'Andrew Lee',
        championEmail: 'andrew-john_lee@astro.com.my'
    },
    {
        pK: 'INO',
        sK: 'ROTATION',
        data: 'elective',
        name: 'Innovation',
        duration: 2,
        category: 'elective',
        capacity: 2,
        championName: 'Farzhan',
        championEmail: 'mohamad-farzan_mohamad-fadhlillah@astro.com.my'
    },
    {
        pK: 'SYS',
        sK: 'ROTATION',
        data: 'elective',
        name: 'Systems',
        duration: 2,
        category: 'elective',
        capacity: 2,
        championName: 'Mee Ting',
        championEmail: 'mee-tin_ea@astro.com.my'
    },
    {
        pK: 'SEC',
        sK: 'ROTATION',
        data: 'elective',
        name: 'Security',
        duration: 1,
        category: 'elective',
        capacity: 2,
        championName: 'Kok Hoe',
        championEmail: 'kok-hoe_wong@astro.com.my'
    },
    {
        pK: 'BRO',
        sK: 'ROTATION',
        data: 'elective',
        name: 'Broadcast',
        duration: 3,
        category: 'elective',
        capacity: 2,
        championName: 'Niven',
        championEmail: 'nivendran_veerappan@astro.com.my'
    },
]

module.exports = seed;

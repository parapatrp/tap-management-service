seed = [
    // User
    {
        pK: 'mdyminde',
        sK: 'USER',
        data: 'active#2017-05-01',
        role: "regular",
        displayName: 'Min Dee',
        joinDate: '2017-05-01',
        electives: ['ARC', 'ANA', 'BRO', 'SYS', 'SEC'],
        status: 'active',
        mentorName: 'Siew Mun',
        mentorEmail: 'siew-mun_chow@astro.com.my'
    },
    {
        pK: 'sjtsoonj',
        sK: 'USER',
        data: 'active#2017-08-01',
        role: "regular",
        displayName: 'Soon Jin',
        joinDate: '2017-08-01',
        electives: ['BRO', 'INO', 'SEC', 'SYS', 'ARC'],
        status: 'active',
        mentorName: 'George Au',
        mentorEmail: 'george_au@astro.com.my'
    },
    {
        pK: 'jtthavaku',
        sK: 'USER',
        data: 'active#2017-08-01',
        role: "regular",
        displayName: 'Thava',
        joinDate: '2017-08-01',
        electives: ['ARC', 'ANA', 'BRO', 'SYS', 'SEC'],
        status: 'active',
        mentorName: 'Eva',
        mentorEmail: 'piramila_poovan@astro.com.my'
    },
    {
        pK: 'hhkahmad',
        sK: 'USER',
        data: 'active#2017-09-01',
        role: "regular",
        displayName: 'Akmal 1.0',
        joinDate: '2017-09-01',
        electives: ['BRO', 'INO', 'SEC', 'SYS', 'ARC'],
        status: 'active',
        mentorName: 'Fariz',
        mentorEmail: 'fariz_salim@astro.com.my'
    },
    {
        pK: 'cpspeish',
        sK: 'USER',
        data: 'active#2017-09-01',
        role: "regular",
        displayName: 'Brianna',
        joinDate: '2017-09-01',
        electives: ['ARC', 'ANA', 'BRO', 'SYS', 'SEC'],
        status: 'active',
        mentorName: 'Peter',
        mentorEmail: 'peter_koay@astro.com.my'
    },
    {
        pK: 'lwyweiye',
        sK: 'USER',
        data: 'active#2018-01-01',
        role: "regular",
        displayName: 'Chris Lim',
        joinDate: '2018-01-01',
        electives: ['BRO', 'INO', 'SEC', 'SYS', 'ARC'],
        status: 'active',
        mentorName: 'Joel Wong',
        mentorEmail: 'joel-soon-lee_wong@astro.com.my'
    },
    {
        pK: 'oaarakmal',
        sK: 'USER',
        data: 'active#2018-02-01',
        role: "regular",
        displayName: 'Akmal 2.0',
        joinDate: '2018-02-01',
        electives: ['ARC', 'ANA', 'BRO', 'SYS', 'SEC'],
        status: 'active',
        mentorName: 'Jamie Kee',
        mentorEmail: 'jamie_kee@astro.com.my'
    },
    {
        pK: 'smcsooma',
        sK: 'USER',
        data: 'active#2018-11-01',
        role: "regular",
        displayName: 'Soo May',
        joinDate: '2018-11-01',
        electives: ['BRO', 'INO', 'SEC', 'SYS', 'ARC'],
        status: 'active',
        mentorName: 'Daphne Tee',
        mentorEmail: 'chui-may_tee@astro.com.my'
    },
    {
        pK: 'aitjiawe',
        sK: 'USER',
        data: 'active#2018-11-01',
        role: "regular",
        displayName: 'Jia Wei',
        joinDate: '2018-11-01',
        electives: ['BRO', 'INO', 'SEC', 'SYS', 'ARC'],
        status: 'active',
        mentorName: 'Daphne Tee',
        mentorEmail: 'chui-may_tee@astro.com.my'
    },
    {
        pK: 'ttctaute',
        sK: 'USER',
        data: 'active#2019-01-01',
        role: "regular",
        displayName: 'Jia Wei',
        joinDate: '2019-01-01',
        electives: ['ARC', 'ANA', 'BRO', 'SYS', 'SEC'],
        status: 'active',
        mentorName: 'Daphne Tee',
        mentorEmail: 'chui-may_tee@astro.com.my'
    },
]

module.exports = seed;

global.fetch = require('node-fetch')
const AWS = require('aws-sdk');


const { Auth } = require('./auth');

module.exports.handler = async (event, context, callback) => {
    const data = JSON.parse(event.body)
    console.log(data)
    const { username, oldPassword, newPassword } = data

    let res = {}

    try {
        const result = await Auth.changePassword(username, oldPassword, newPassword)

        res = {
            statusCode: 200,
            headers: {
                "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
            },
            body: JSON.stringify({
                msg: 'Password has changed',
                response: result
            })
        }
        callback(null, res)
    } catch(err) {
        res = {
            statusCode: 400,
            headers: {
                "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
            },
            body: JSON.stringify(err)
        }
        console.log(res)
        callback(res)
    }
};

'use strict';
const AWS = require('aws-sdk');
const moment = require('moment');
const { getRotationInfo, listUsers, getSchedule, getRotationConfig,
        filterUsersToBeReminded, updateRotationStatus, listRotations } = require('./utils');

const ses = new AWS.SES({region: process.env.SES_REGION});

module.exports.handler = async (event, context, callback) => {
    let res = {}

    const tapReminderParams = {
        Source: process.env.SRC_EMAIL,
        Destination: {
          ToAddresses: [process.env.DST_EMAIL]
        },
        Template: 'TAPLeadReminder'
    };

    const rotationReminderParams = {
        Source: process.env.SRC_EMAIL,
        Destination: {
          ToAddresses: [process.env.DST_EMAIL]
        },
        Template: 'RotationReminder'
    };

    try {
        const schedule = await getSchedule('latestSchedule');
        const users = await listUsers();
        let rotations = await listRotations();
        rotations = Object.assign({}, ...rotations.Items.map(({pK, name}) => ({[pK]: {name}})))
        console.log(rotations)
        // Retrive user id that needed to be reminded
        const toBeReminded = filterUsersToBeReminded(schedule.Item.schedule);
        const usersToBeReminded = users.Items.filter((user) => toBeReminded.includes(user.pK));
        // const usersToBeReminded = users.Items.filter((user) => user.pK === 'lwyweiye');
        console.log(usersToBeReminded);

        usersToBeReminded.forEach(async (user) => {
            const userSchedule = schedule.Item.schedule[user.pK]
            const { currentRotation, nextRotation } = getRotationInfo(userSchedule);
            const currentRotationInfo = currentRotation.rotationId ? await getRotationConfig(currentRotation.rotationId) : '';
            const nextRotationInfo = nextRotation.rotationId ? await getRotationConfig(nextRotation.rotationId) : '';

            let completedRotations = userSchedule.map(updateRotationStatus)
                .filter(rotation => rotation.status === 'COMPLETED' || rotation.status === 'ONGOING')
                .map(rotation => {
                    return rotations[rotation.rotationId] ? rotations[rotation.rotationId].name : rotation.rotationId
                })

            console.log(completedRotations)
            const tapReminderData = JSON.stringify({
                protegeName: user.displayName,
                currentRotationEndDate: currentRotation.endDate ? currentRotation.endDate : '',
                nextRotationName: nextRotation.rotationId ? nextRotationInfo.Item.name : ''
            })

            const rotationReminderData = JSON.stringify({
                protegeName: user.displayName,
                nextRotationName: nextRotation.rotationId ? nextRotationInfo.Item.name : '',
                nextRotationStartDate: nextRotation.startDate ? nextRotation.startDate : '',
                nextRotationEndDate: nextRotation.endDate
                    ? moment(nextRotation.endDate).format("YYYY-MM-DD") : '',
                championName: nextRotationInfo.Item.championName,
                nextRotationPeriod: `${Math.round(moment(nextRotation.endDate, 'YYYY-MM-DD').diff(moment(nextRotation.startDate, 'YYYY-MM-DD'), 'days') / 7)} weeks`,
                noOfCompletedRotation: completedRotations.length,
                completedRotations: completedRotations.join(', ')
            })

            console.log(tapReminderData)
            await ses.sendTemplatedEmail(
                {...tapReminderParams, TemplateData: tapReminderData}).promise()
            console.log(rotationReminderData)
            await ses.sendTemplatedEmail(
                {...rotationReminderParams, TemplateData: rotationReminderData}).promise()
        })
        res = {
            statusCode: 200,
            body: JSON.stringify({
                message: 'Email sent succesfully !'
            }),
        }
        callback(null, res)
    } catch (err) {
        res = {
            statusCode: 502,
            body: JSON.stringify({
                message: 'Email failed to be sent !',
                error: err
            }),
        }
        callback(err)
    }
};

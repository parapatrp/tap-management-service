const AWS = require('aws-sdk');
const dynamodb = new AWS.DynamoDB.DocumentClient({ region: 'ap-southeast-1'});
const moment = require('moment');

const REMINDER_PERIOD = 7;

Date.prototype.withoutTime = function () {
    const d = new Date(this);
    d.setHours(0, 0, 0, 0);
    return d;
}

const getRotationInfo = (schedule) => {
    let nextRotation = {}
    let currentRotation = {}

    const pendingRotations = schedule.map(updateRotationStatus)
        .filter(rotation => rotation.status === 'PENDING')
    const currentRotations = schedule.map(updateRotationStatus)
        .filter(rotation => rotation.status === 'ONGOING')
    if (pendingRotations.length > 0) {
        nextRotation = pendingRotations
            .sort((a, b) => {
                if (Date.parse(a.startDate) < Date.parse(b.startDate))
                    return -1;
                if (Date.parse(a.startDate) > Date.parse(b.startDate))
                    return 1;
                return 0;
            })[0]
    }
    if (currentRotations.length > 0) {
        currentRotation = currentRotations[0]
    }
    return { currentRotation, nextRotation }
}

const listWorkingSchedule = () => {
    const params = {
        TableName: process.env.AWS_DYNAMODB_TABLE,
        IndexName: 'sK-data-index',
        KeyConditionExpression:"#sK = :sKValue",
        ExpressionAttributeNames: {
            "#sK":"sK",
        },
        ExpressionAttributeValues: {
            ":sKValue": 'WORKING_SCHEDULE',
        }
    }
    return dynamodb.query(params).promise();
}

const listRotations = () => {
    const params = {
        TableName: process.env.AWS_DYNAMODB_TABLE,
        IndexName: 'sK-data-index',
        KeyConditionExpression:"#sK = :sKValue",
        ExpressionAttributeNames: {
            "#sK":"sK",
        },
        ExpressionAttributeValues: {
            ":sKValue": 'ROTATION',
        }
    }
    return dynamodb.query(params).promise();
}

const updateRotationStatus = (rotation) => {
    let status = ''
    const today = new Date()
    const startDate = new Date(rotation.startDate);
    const endDate = new Date(rotation.endDate);
    if (today.withoutTime() >= endDate.withoutTime()) {
        status = 'COMPLETED';
    } else {
        status = today.withoutTime() >= startDate.withoutTime() ? 'ONGOING' : 'PENDING';
    }
    return { ...rotation, status }
}

// Perform DynamoDB batch insert of items
const batchWrite = async (seed) => {
    const params = {
        RequestItems: {}
    };

    const formattedSeed = seed.map(item => {
        return {PutRequest: { Item: item}}
    });
    params.RequestItems[process.env.AWS_DYNAMODB_TABLE] = formattedSeed

    return dynamodb.batchWrite(params).promise()
}

// Filter list of users that should be sent reminder for upcoming rotation
// 1 days in advanced
const filterUsersToBeReminded = (schedule) => {
    const usersToBeReminded = Object.keys(schedule).filter((key) => {
        const userSchedule = schedule[key].map(updateRotationStatus)
            .filter(rotation => rotation.status === 'PENDING')
        if (userSchedule.length > 0) {
            console.log(key)
            // Not setting hours will yield weird result
            const nextRotations = userSchedule
                    .sort((a, b) => {
                        if (Date.parse(a.startDate) < Date.parse(b.startDate))
                            return -1;
                        if (Date.parse(a.startDate) > Date.parse(b.startDate))
                            return 1;
                        return 0;
                    })
            const daysToNextRotation = Math.abs(moment({hours: 0}).diff(nextRotations[0].startDate, 'days'))
            return daysToNextRotation == REMINDER_PERIOD;
        } else {
            return false
        }
    });
    return usersToBeReminded;
}

const getRotationConfig = (rotationId) => {
    const params = {
        TableName: process.env.AWS_DYNAMODB_TABLE,
        Key: { pK: rotationId, sK: 'ROTATION' }
    }
    return dynamodb.get(params).promise();
}

const listUsers = () => {
    const params = {
        TableName: process.env.AWS_DYNAMODB_TABLE,
        IndexName: 'sK-data-index',
        KeyConditionExpression:"#sK = :sKValue",
        ExpressionAttributeNames: {
            "#sK":"sK",
        },
        ExpressionAttributeValues: {
            ":sKValue": 'USER',
        }
    }
    return dynamodb.query(params).promise();
}

const getSchedule = (scheduleId) => {
    const params = {
        TableName: process.env.AWS_DYNAMODB_TABLE,
        Key: { pK: scheduleId, sK: 'SCHEDULE' }
    }
    return dynamodb.get(params).promise();
}

module.exports = { batchWrite, updateRotationStatus, listUsers, getSchedule,
                   filterUsersToBeReminded, getRotationInfo, getRotationConfig, listRotations, listWorkingSchedule
}

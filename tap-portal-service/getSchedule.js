'use strict';
const AWS = require('aws-sdk');
const { updateRotationStatus } = require('./utils');

const dynamodb = new AWS.DynamoDB.DocumentClient({ region: 'ap-southeast-1'});

module.exports.handler = (event, context, callback) => {
    const { scheduleId } = event.queryStringParameters
    const params = {
        TableName: process.env.AWS_DYNAMODB_TABLE,
        Key: { pK: scheduleId, sK: 'SCHEDULE' }
    }

    dynamodb.get(params, (err, data) => {
        let res = {}
        if (err) {
            console.log(err)
            res = {
                statusCode: 400,
                error: JSON.stringify(err)
            }
            callback(res)
        } else {
            const schedule = data['Item']
            const timetable = schedule['schedule']
            Object.keys(timetable).map((key, index) => {
                console.log(timetable[key])
                timetable[key] = timetable[key].map(updateRotationStatus);
            });
            schedule['schedule'] = timetable
            res = {
                statusCode: 200,
                headers: {
                    "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
                },
                body: JSON.stringify({
                    message: 'Schedule fetched successfully',
                    schedule: schedule
                })
            }
            callback(null, res)
        }
    });
};

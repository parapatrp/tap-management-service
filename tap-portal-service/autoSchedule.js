const AWS = require('aws-sdk');
const { listUsers, listWorkingSchedule, getRotationConfig, getSchedule } = require('./utils');
const moment = require('moment');

const lambda = new AWS.Lambda({region: 'ap-southeast-1'});

module.exports.handler = async (event, context, callback) => {
    const { Items: users } = await listUsers();
    const { Item: latestSchedule } = await getSchedule('latestSchedule');
    const { Item: initialRotation } = await getRotationConfig('PMO');
    const workingSchedule = latestSchedule.schedule;
    const newUsers = users.filter((user) => user['role'] === 'regular' && !Object.keys(workingSchedule).includes(user['pK']))
    if (newUsers.length > 0) {
        workingSchedule = Object.assign(workingSchedule, ...newUsers.map(({pK, joinDate}) => ({[pK]: [{rotationId: 'PMO', startDate: joinDate, endDate: moment(joinDate).add(4*initialRotation.duration, 'w').format('YYYY-MM-DD')}]})))
    }

    const params = {
        FunctionName: process.env.SCHEDULE_LAMBDA,
        InvocationType: 'Event',
        Payload: JSON.stringify({
            initSchedule: workingSchedule,
            isManual: false
        })
    }

    let res = {}
    try {
        let invokedRes = await lambda.invoke(params).promise()
        res = {
            statusCode: 200,
            headers: {
                "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
            },
            body: JSON.stringify({
                msg: 'Request to generate schedule has been received',
                result: users
            })
        }
        callback(null, res)
    } catch(err) {
        console.log(err)
        res = {
            statusCode: 400,
            body: JSON.stringify(err)
        }
        callback(res)
    }
};

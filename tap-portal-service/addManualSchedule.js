const AWS = require('aws-sdk');
const { listRotations } = require('./utils');
const uuid = require('uuid/v4');
const moment = require('moment');

const dynamodb = new AWS.DynamoDB.DocumentClient({ region: 'ap-southeast-1'});

const getConflictAssignments = (schedule, rotations) => {
    const assignments = 
    Object.keys(schedule).forEach((userId) => {
        schedule[userId].reduce((tally, rotation) => {
            const duration = moment(rotation.startDate, 'YYYY-MM-DD').diff(moment(rotation.endDate, 'YYYY-MM-DD'), 'month');
        }, {})
    })
}

module.exports.handler = async (event, context, callback) => {
    const { schedule } = JSON.parse(event.body);
    const createdAt = new Date();
    const { Items: rotations } = await listRotations();
    const conflictAssignments = getConflictAssignments(schedule, rotations)

    let res = {}

    const params = {
        TableName: process.env.AWS_DYNAMODB_TABLE,
        Item: {
            pK: uuid(),
            sK: 'PENDING_SCHEDULE',
            createdAt: createdAt.toISOString()
        }
    };

    try {
        await dynamodb.put(params).promise()
        res = {
            statusCode: 200,
            body: JSON.stringify({
                msg: 'Schedules successfully added'
            })
        }
        callback(null, res)
    } catch(err) {
        console.log(err)
        res = {
            statusCode: 400,
            body: JSON.stringify(err)
        }
        callback(res)
    }
};

const AWS = require('aws-sdk');

const dynamodb = new AWS.DynamoDB.DocumentClient({ region: 'ap-southeast-1'});

module.exports.handler = async (event, context, callback) => {
    const { scheduleId } = event.pathParameters
    const scheduleParams = {
        TableName: process.env.AWS_DYNAMODB_TABLE,
        Key: { pK: scheduleId, sK: 'PENDING_SCHEDULE' }
    }

    let res = {}

    try {
        const currentSchedule = await dynamodb.get(scheduleParams).promise();

        const updateParams = Object.assign(currentSchedule.Item, { sK: 'WORKING_SCHEDULE' });
        await dynamodb.put({TableName: process.env.AWS_DYNAMODB_TABLE, Item: updateParams}).promise()

        const latestScheduleParams = Object.assign(currentSchedule.Item, { pK: 'latestSchedule', sK: 'SCHEDULE'})
        await dynamodb.put({TableName: process.env.AWS_DYNAMODB_TABLE, Item: latestScheduleParams}).promise()

        res = {
            statusCode: 200,
            body: JSON.stringify({
                msg: `Pending Schedule ${scheduleId} has been confirmed`
            })
        }
        callback(null, res)
    } catch(err) {
        console.log(err)
        res = {
            statusCode: 400,
            body: JSON.stringify(err)
        }
        callback(res)
    }
};

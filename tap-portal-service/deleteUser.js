const AWS = require('aws-sdk');
const { Auth } = require('./auth');

const dynamodb = new AWS.DynamoDB.DocumentClient({ region: 'ap-southeast-1'});

module.exports.handler = async (event, context, callback) => {
    const { userId } = event.pathParameters
    const params = {
        TableName: process.env.AWS_DYNAMODB_TABLE,
        Key: { pK: userId, sK: 'USER' }
    }
    let res = {}

    try {
        await Auth.adminDeleteUser(userId);
        await dynamodb.delete(params).promise();
        res = {
            statusCode: 200,
            headers: {
                "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
            },
            body: JSON.stringify({
                msg: `User ${userId} successfully deleted`
            })
        }
        callback(null, res)
    } catch(err) {
        console.log(err)
        res = {
            statusCode: 400,
            body: JSON.stringify(err)
        }
        callback(res)
    }
};

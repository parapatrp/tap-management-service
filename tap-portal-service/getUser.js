'use strict';
const AWS = require('aws-sdk');

const dynamodb = new AWS.DynamoDB.DocumentClient({ region: 'ap-southeast-1'});

module.exports.handler = (event, context, callback) => {
    const { userId } = event.pathParameters
    const params = {
        TableName: process.env.AWS_DYNAMODB_TABLE,
        Key: { pK: userId, sK: 'USER' }
    }

    dynamodb.get(params, (err, data) => {
        let res = {}
        if (err) {
            console.log(err)
            res = {
                statusCode: 400,
                headers: {
                    "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
                },
                body: JSON.stringify(err)
            }
            callback(res)
        } else {
            res = {
                statusCode: 200,
                headers: {
                    "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
                },
                body: JSON.stringify({
                    msg: `User ${userId} fetched successfully`,
                    data: data['Item']
                })
            }
            callback(null, res)
        }
    });
};

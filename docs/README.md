# ATAS: Automated TA Scheduling System

## Getting Started

### Setup Git

Follow the instructions [here](https://www.atlassian.com/git/tutorials/install-git) to install git for your respective OS

### Clone Repositories

1. Clone TAP-Portal (client-side) repository
```bash
git clone https://github.com/akmaluddin/scheduling_tool
```

2. Clone TAP-Services (server-side) repository
```bash
git clone https://parapatrp@bitbucket.org/parapatrp/tap-management-service.git
```

### Setup AWS Credentials (needed for deployment)

1. Request for AWS Credentials from hisyam_shukri@astro.com.my
2. Install [**AWS Cli**](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html)
3. Configure profile for project with `access-key-id` and `secret-access-key` obtained from AWS IAM console
```bash
aws configure --profile tap
```

### Setup Postman

1. Import [collection](https://bitbucket.org/parapatrp/tap-management-service/raw/6f41b62e19e290cf039e6e571fed7985050c5760/swagger/TAP_API_collection.json) and [environment](https://bitbucket.org/parapatrp/tap-management-service/raw/6f41b62e19e290cf039e6e571fed7985050c5760/swagger/TAP_postman_environment.json) file

### Install Serverless

```bash
npm install -g serverless
```

Happy Development !!

## Development

Individual developer will work on short-lived branch for each feature/issue that will be constantly merged into `staging` environment via pull request

### Languages & Frameworks

- TAP Portal (client-side)
    - Javascript
    - [React](https://reactjs.org/) and [Material UI](https://material-ui.com/)
    - [Redux for state mangement](https://redux.js.org/) and [Redux Saga for side-effects](https://redux-saga.js.org/)
- TAP Services
    - Python 3.6 for `scheduling services`
    - Javascript for `portal services`
    - [Serverless: toolkit for building serverless applications](https://serverless.com/)
    - [python-constraint: constraint satisfaction problem solver (scheduling)](https://labix.org/python-constraint)

## Architecture

![Architecture Diagram](architecture.png)

The client-side is developed as a SPA which communicate to the services REST endpoint via API Gateway. Each service is served as a Lambda function and protected via API-KEY
> **TODO** Change to use ID Token in the future

## Deployment

### TAP Portal (client-side)

1. Make sure latest version of the code is pulled
```bash
git pull && git checkout staging
```
2. Build 
```bash
yarn install && yarn build
```
3. Push to S3 bucket
```bash
aws s3 sync ./build s3://astro-tap-web --profile tap
```
> **NOTE** Ensure proper credentials is setup and the profile name corresponds to the profile name used during configuration step

### TAP Services (server-side)

1. Change directory to either `tap-portal-services` or `tap-scheduling-services`
2. Deploy
```bash
sls deploy -v --aws-profile tap
```

## Data Model

Uses AWS DynamoDB as primary storage. Login information is handled by Cognito which maintained its own database. Both databases are linked via `userId`

There are 4 primary data types: SCHEDULE, ROTATION, USER, SESSION

Some intro to DynamoDB can be found [here](https://aws.amazon.com/dynamodb/). These few columns are mandatory:

- `pK`: primary key to identify item
- `sK`: sort key used to identify data type e.g. SCHEDULE, ROTATION, USEr and SESSION
- `data`: used for further sorting 


## Authentication and Authorization

### Authentication (Cognito)

Authentication is handled by AWS Cognito User Pool using `AUTHORIZATION CODE GRANT` flow

Below is the login flow:

- A session will be created upon first time login. `Session Token` and `ID Token` will be stored in  client local storage
- Session token will lasts for 1 hour
- Upon session expiration, the service will attempt to refresh the ID Token (provided the refresh token is still alive (30 days))
- A new id token and session token will be returned to client

#### References

- [OAuth 2.0 and OpenID Connect in plain English](https://www.youtube.com/watch?v=996OiexHze0)

### Authorization

- the `role: regular or admin` is used to provide fine grained role based access to our resources


## Scheduling

Uses `python-constraint` library to solve scheduling problem using a **Backtracking solver**

### Hard Constraint

Every rotation to a protege must be unique

### Soft Constraints

- Prefer core first rotation
- Prefer more 'spread' assignments
